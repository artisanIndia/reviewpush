/**
 * Created by shahrukhkhan on 25/01/17.
 */

$('#select_all').click(function () {
    $('input[name=studentId]').prop('checked', true);
});

$('#export').click(function()
{
    var checkedIds = [];
    $("input:checkbox").each(function(){
        var $this = $(this);

        if($this.is(":checked")){
            checkedIds.push($this.attr("value"));
        }
    });

    var token = $('meta[name=csrf-token]').attr("content");
    $.post('/export', {'ids' : checkedIds, '_token' : token}, function(data)
    {
        var parsed = JSON.parse(data);

        window.open('/download/'+parsed);
    })
});