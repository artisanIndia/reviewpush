<?php

namespace App\Http\Controllers;

use App\Models\Students;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\DocBlock\Tags\Formatter;

class ExportController extends Controller
{
    public function __construct()
    {

    }

    public function welcome()
    {
        return view('hello');
    }

    /**
     * View all students found in the database
     */
    public function viewStudents()
    {
        $students = Students::with('course')->get();
        return view('view_students', compact(['students']));
    }

    /**
     * Exports all student data to a CSV file
     */
    public function exportStudentsToCSV(Request $request)
    {
        $students = Students::whereIn('id', $request->ids)->with('course')->get();

        $filename = str_random(15);
        $filename .= '.csv';

        $handle = fopen(public_path($filename), 'w+');
        fputcsv($handle, array('Forename', 'Surname', 'Email', 'University', 'Course'));

        foreach($students as $row) {
            fputcsv($handle, array($row['firstname'], $row['surname'], $row['email'], $row['course']['university'], $row['course']['course_name']));
        }

        fclose($handle);

        return json_encode($filename);

    }

    public function downloadFile($filename)
    {
        $headers = $headers = array(
            'Content-Type' => 'text/csv',
        );

        return response()->download(public_path($filename), 'CSV Download', $headers);
    }

    /**
     * Exports the total amount of students that are taking each course to a CSV file
     */
    public function exporttCourseAttendenceToCSV()
    {

    }
}
